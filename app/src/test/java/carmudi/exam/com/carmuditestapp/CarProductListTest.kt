package carmudi.exam.com.carmuditestapp

import carmudi.exam.com.carmuditestapp.presentation.car.CarProductListContract
import carmudi.exam.com.carmuditestapp.presentation.car.CarProductListPresenter
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.Disposable
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.BeforeClass
import org.junit.Test
import java.util.concurrent.Executor
import java.util.concurrent.TimeUnit


class CarProductListTest {

    companion object {
        @BeforeClass
        @JvmStatic
        fun setup() {
            val immediate = object : Scheduler() {
                override fun scheduleDirect(@NonNull run: Runnable, delay: Long, @NonNull unit: TimeUnit): Disposable {
                    // this prevents StackOverflowErrors when scheduling with a delay
                    return super.scheduleDirect(run, 0, unit)
                }

                override fun createWorker(): Scheduler.Worker {
                    return ExecutorScheduler.ExecutorWorker(Executor { it.run() })
                }
            }

            RxJavaPlugins.setInitIoSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitComputationSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitNewThreadSchedulerHandler { scheduler -> immediate }
            RxJavaPlugins.setInitSingleSchedulerHandler { scheduler -> immediate }
            RxAndroidPlugins.setInitMainThreadSchedulerHandler { scheduler -> immediate }
        }
    }


    @Test
    fun load_PresenterMustCallRefreshProductOnSuccess() {


        val viewModel: CarProductListContract.ViewModel = mock()

        var presenter = CarProductListPresenter(viewModel)
        presenter.loadProductList()

        verify(viewModel).refreshCarProducts()

    }

    @Test
    fun load_PresenterMustCallFinishLoadingMoreDataOnSuccess() {


        val viewModel: CarProductListContract.ViewModel = mock()

        var presenter = CarProductListPresenter(viewModel)
        presenter.loadProductList()

        verify(viewModel).finishLoadingMoreData()

    }


    @Test
    fun load_PresenterMustCallRefreshCarProductsOnSuccess() {
        val viewModel: CarProductListContract.ViewModel = mock()
        val presenter = CarProductListPresenter(viewModel)
        presenter.loadProductList()
        verify(viewModel).refreshCarProducts()
    }

    @Test
    fun load_PresenterMustCallLoadingCarProductData() {
        val viewModel: CarProductListContract.ViewModel = mock()
        val presenter = CarProductListPresenter(viewModel)
        presenter.loadProductList()
        verify(viewModel).loadingCarProductData()
    }

    @Test
    fun doneLoad_PresenterMustCallfinishCarProductDataLoading() {
        val viewModel: CarProductListContract.ViewModel = mock()
        val presenter = CarProductListPresenter(viewModel)
        presenter.loadProductList()
        verify(viewModel).finishCarProductDataLoading()
    }

    @Test
    fun refresh_PresenterMustCallRefreshCarProductsOnSuccess() {

        val viewModel: CarProductListContract.ViewModel = mock()

        val presenter = CarProductListPresenter(viewModel)
        presenter.loadProductList()

        verify(viewModel).refreshCarProducts()

    }

    @Test
    fun sort_PresenterMustCallRefreshCarProductsOnSuccess() {
        val viewModel: CarProductListContract.ViewModel = mock()
        val presenter = CarProductListPresenter(viewModel)
        presenter.loadProductList()
        verify(viewModel).refreshCarProducts()
    }


}