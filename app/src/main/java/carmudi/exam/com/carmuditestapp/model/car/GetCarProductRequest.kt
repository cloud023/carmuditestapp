package carmudi.exam.com.carmuditestapp.model.car

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlin.math.max

/**
 * Created by Jayson Duran on 6/20/2018.
 * Data Map for Get Product List Request
 */
class GetCarProductRequest(@Expose
                           @SerializedName("sort")
                           private var sort: String, @Expose
                           @SerializedName("page")
                           private var page: Int, @Expose
                           @SerializedName("maxItems")
                           private var maxItems: Int) {


    fun getPage(): Int {
        return page
    }

    fun getMaxItems(): Int {
        return maxItems
    }

    fun getSort(): String {
        return sort
    }

}