package carmudi.exam.com.carmuditestapp.presentation.car

import carmudi.exam.com.carmuditestapp.model.car.GetCarProductResponse

interface CarProductListContract {

    interface ViewModel {
        fun refreshCarProducts()
        fun finishLoadingMoreData()
        fun loadingCarProductData()
        fun finishCarProductDataLoading()
        fun errorCarProductLoading()
    }

    interface Presenter {
        fun loadProductList()
        fun loadProductWithSorting(sorting: String)
        fun refreshProductList()
        fun getProductList(): List<GetCarProductResponse.Results>?
        fun getProductListCount(): Int
        fun onDestroyActivity()
        fun getCarProductFilters(): ArrayList<String>

    }

}