package carmudi.exam.com.carmuditestapp.model.car

import com.google.gson.annotations.SerializedName
import java.io.Serializable


/**
 * This class interpret and convert the JSON data coming from the webservice.
 * Also, utilizes GSON library which eases the JSON parsing significantly.
 * I love GSON Library :)
 */
class GetCarProductResponse {

    @SerializedName("success")
    private var success: Boolean = false

    @SerializedName("metadata")
    private var metaData: MetaData = MetaData()

    fun getSuccess(): Boolean {
        return success
    }

    fun getResponseData(): ArrayList<Results> {
        return metaData.getResults()
    }


    private class MetaData {

        @SerializedName("product_count")
        private var productCount = 0

        @SerializedName("results")
        private var results = ArrayList<Results>()

        fun getResults(): ArrayList<Results> {
            return results
        }

    }

    class Results : Serializable {

        @SerializedName("data")
        private var data: CarProduct = CarProduct()

        @SerializedName("images")
        private var images = ArrayList<Images>()


        fun getData(): CarProduct {
            return data
        }

        fun getFirstImageUrl(): String {
            return images[0].getUrl()
        }

    }

    class Images {

        @SerializedName("url")
        private var url: String = ""

        fun getUrl(): String {
            return url
        }

    }

}