package carmudi.exam.com.carmuditestapp.model.remote

class ApiEndPoint {

    companion object {

        val DEFAULT_CONNECT_TIMEOUT_SECONDS = 15L
        val DEFAULT_READ_TIMEOUT_SECONDS = 15L

        val BASE_API_URL = "https://www.carmudi.co.id/api/"
        val CAR_LIST : String = "$BASE_API_URL/cars/"


    }

}