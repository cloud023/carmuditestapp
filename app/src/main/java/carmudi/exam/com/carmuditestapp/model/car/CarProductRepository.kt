package carmudi.exam.com.carmuditestapp.model.car

import io.reactivex.Single


/**
 * Handles all Data related calls for now it is simple because
 * the data is only coming from the webservice for now.
 */
class CarProductRepository {

    companion object {

        fun getCarProductListFromApi(request: GetCarProductRequest): Single<GetCarProductResponse> {
            return CarProductApi.getProductList(request)
        }

    }


}