package carmudi.exam.com.carmuditestapp.presentation

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.LinearLayoutManager


abstract class LoadMoreOnScrollListener : RecyclerView.OnScrollListener() {

    /**
     * The total number of items in the dataset after the last load
     */
    private var mPreviousTotal = 0

    /**
     * True if we are still waiting for the last set of data to load.
     */
    private var mLoading = true

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = recyclerView!!.childCount
        val totalItemCount = recyclerView.layoutManager.itemCount
        val firstVisibleItem = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()

        if (mLoading) {
            if (totalItemCount > mPreviousTotal) {
                mLoading = false
                mPreviousTotal = totalItemCount
            }
        }

        val visibleThreshold = 20

        if (!mLoading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {

            onLoadMore()

            mLoading = true
        }
    }

    // Call this method whenever performing new searches on in onRefresh() if using SwipeRefresh
    fun resetState() {
        this.mPreviousTotal = 0
        this.mLoading = true
    }

    abstract fun onLoadMore()

}