package carmudi.exam.com.carmuditestapp.presentation.car

import android.util.Log
import carmudi.exam.com.carmuditestapp.model.car.CarProductRepository
import carmudi.exam.com.carmuditestapp.model.car.GetCarProductRequest
import carmudi.exam.com.carmuditestapp.model.car.GetCarProductResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*

class CarProductListPresenter(viewModel: CarProductListContract.ViewModel) : CarProductListContract.Presenter {

    private var mViewModel: CarProductListContract.ViewModel = viewModel
    private var mCarProductList = ArrayList<GetCarProductResponse.Results>()
    private var mCompositeDisposable: CompositeDisposable = CompositeDisposable()
    private var mCarProductSorting: String = ""
    private val CAR_PRODUCT_MAX_ITEM = 20

    /**
     * Load Car Products from api which is trigerred using the CarProductRepository
     */
    override fun loadProductList() {

        //Get Request Data this is handy for both POST and GET request calls
        val request = GetCarProductRequest(getProductSorting(), getCurrentPage(), CAR_PRODUCT_MAX_ITEM)

        mViewModel.loadingCarProductData()

        //All RxJava calls must be added to the composite disposable object for proper disposal
        mCompositeDisposable.add(CarProductRepository.getCarProductListFromApi(request)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSuccess { response ->
                    if (response.getSuccess()) {
                        if (mCarProductList.size == 0) {
                            this.mCarProductList = response.getResponseData()
                        } else {
                            this.mCarProductList.addAll(response.getResponseData())
                        }
                        mViewModel.refreshCarProducts()
                        mViewModel.finishLoadingMoreData()
                        mViewModel.finishCarProductDataLoading()
                    }


                }
                .onErrorReturn {
                    mViewModel.errorCarProductLoading()
                    mViewModel.finishCarProductDataLoading()
                    GetCarProductResponse()

                }
                .doOnError({
                    Log.d("ERROR", "error" + it.message)
                })
                .subscribe())

    }

    /**
     * If No Filter specified default to newest product sorting
     */
    private fun getProductSorting(): String {
        return if (mCarProductSorting.isEmpty() || mCarProductSorting == "No Filter") "newest" else mCarProductSorting
    }


    override fun refreshProductList() {
        mCarProductList = ArrayList()
        mViewModel.refreshCarProducts()
        loadProductList()
    }

    /**
     * Compute paging based on the number of items on the list
     */
    private fun getCurrentPage(): Int {
        return if (mCarProductList.size == 0) 1 else mCarProductList.size / CAR_PRODUCT_MAX_ITEM
    }

    override fun getProductList(): List<GetCarProductResponse.Results>? {
        return mCarProductList
    }

    override fun getProductListCount(): Int {
        return mCarProductList.size
    }

    /**
     * For simplicity I put it here but usually this is stored in storage
     */
    override fun getCarProductFilters(): ArrayList<String> {

        return arrayListOf("No Filter", "oldest", "newest", "price-low", "price-high", "high-mileage", "low-mileage")

    }

    /**
     * Trigger loading of products with sorting supplied
     */
    override fun loadProductWithSorting(sorting: String) {
        this.mCarProductSorting = sorting
        mCarProductList = ArrayList()
        mViewModel.refreshCarProducts()
        loadProductList()
    }

    /**
     * Upon calling on Destroy on Activity lifecycle
     * Dispose all RxJava2 Disposable objects using CompositeDisposable
     * to avoid any leaks and undesired background thread processes.
     */
    override fun onDestroyActivity() {
        mCompositeDisposable.dispose()
    }

}