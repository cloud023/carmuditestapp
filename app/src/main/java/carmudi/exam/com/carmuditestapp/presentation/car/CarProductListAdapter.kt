package carmudi.exam.com.carmuditestapp.presentation.car

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import carmudi.exam.com.carmuditestapp.databinding.RecyclerCarProductListItemBinding

class CarProductListAdapter(carProductListPresenter: CarProductListPresenter) : RecyclerView.Adapter<CarProductListAdapter.ViewHolder>() {

    private var mCarProductListPresenter = carProductListPresenter

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {

        val viewDataBinder = RecyclerCarProductListItemBinding.inflate(LayoutInflater.from(parent?.context),
                parent, false)

        return ViewHolder(viewDataBinder)

    }

    fun doRefreshData() {
        notifyDataSetChanged()
        notifyItemRangeChanged(0, mCarProductListPresenter.getProductListCount())
    }

    override fun getItemCount(): Int {
        return mCarProductListPresenter.getProductListCount()
    }

    /**
     * Android Data Binding by Default handles ViewHolder pattern assigning and recycling
     * no need to handle it here.
     */
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.messageDataBinder.carProductResults = mCarProductListPresenter.getProductList()?.get(position)
        holder.messageDataBinder.executePendingBindings()
    }


    class ViewHolder(itemBinder: RecyclerCarProductListItemBinding) : RecyclerView.ViewHolder(itemBinder.root) {

        val messageDataBinder: RecyclerCarProductListItemBinding = itemBinder
    }
}