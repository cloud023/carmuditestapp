package carmudi.exam.com.carmuditestapp.presentation.car

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Toast
import carmudi.exam.com.carmuditestapp.R
import carmudi.exam.com.carmuditestapp.presentation.LoadMoreOnScrollListener
import kotlinx.android.synthetic.main.fragment_car_product_list.view.*


class CarProductListFragment : Fragment(), CarProductListContract.ViewModel {


    private var mProductListPresenter: CarProductListPresenter? = null
    private var mCarProductListAdapter: CarProductListAdapter? = null
    private var mCarProductFilterAdapter: ArrayAdapter<String>? = null

    companion object {
        val CAR_PRODUCT_LIST_FRAGMENT_TAG = "car_product_list_product"
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_car_product_list, container, false)

        mProductListPresenter = CarProductListPresenter(this)

        view.recycler_fragment_car_product_list.itemAnimator = DefaultItemAnimator()
        view.recycler_fragment_car_product_list.layoutManager = LinearLayoutManager(context)

        mCarProductListAdapter = CarProductListAdapter(mProductListPresenter!!)
        mCarProductFilterAdapter = ArrayAdapter(context, android.R.layout.simple_list_item_1, mProductListPresenter!!.getCarProductFilters())

        view?.recycler_fragment_car_product_list?.adapter = mCarProductListAdapter
        view?.spin_fragment_car_product_list_filter?.adapter = this.mCarProductFilterAdapter

        view.recycler_fragment_car_product_list?.addOnScrollListener(mLoadMoreScrollListener)

        view.swipe_fragment_car_product_list_refresh.setOnRefreshListener(mRefreshDataListener)

        view.spin_fragment_car_product_list_filter.onItemSelectedListener = mOnFilterCarProductListener

        return view
    }


    private var mOnFilterCarProductListener: OnItemSelectedListener = object : OnItemSelectedListener {

        override fun onNothingSelected(p0: AdapterView<*>?) {

        }

        override fun onItemSelected(filterAdapter: AdapterView<*>?, spinFilter: View?, p2: Int, p3: Long) {
            mProductListPresenter?.loadProductWithSorting(filterAdapter?.selectedItem.toString())
            mLoadMoreScrollListener.resetState()
        }

    }

    private var mLoadMoreScrollListener: LoadMoreOnScrollListener = object : LoadMoreOnScrollListener() {
        override fun onLoadMore() {
            view?.txt_fragment_car_product_list_loadmore?.visibility = View.VISIBLE
            mProductListPresenter?.loadProductList()
        }
    }

    private var mRefreshDataListener: SwipeRefreshLayout.OnRefreshListener = SwipeRefreshLayout.OnRefreshListener {
        mProductListPresenter?.refreshProductList()
        mLoadMoreScrollListener.resetState()
    }


    override fun finishLoadingMoreData() {
        view?.txt_fragment_car_product_list_loadmore?.visibility = View.GONE
        view?.swipe_fragment_car_product_list_refresh?.isRefreshing = false
    }

    /**
     * Trigger Adapter to refresh productlist
     */
    override fun refreshCarProducts() {
        mCarProductListAdapter?.doRefreshData()
    }

    /**
     * When activity was destroy execute
     * RxJava disposable function this is needed to avoid memory leaks
     */
    override fun onDestroy() {
        mProductListPresenter?.onDestroyActivity()
        super.onDestroy()
    }

    override fun loadingCarProductData() {
        view?.progressbar_fragment_car_product_list?.visibility = View.VISIBLE
    }

    override fun finishCarProductDataLoading() {
        view?.progressbar_fragment_car_product_list?.visibility = View.GONE
    }

    override fun errorCarProductLoading() {
        view?.swipe_fragment_car_product_list_refresh?.isRefreshing = false
        Toast.makeText(context, getString(R.string.error_unable_to_load_data),
                Toast.LENGTH_LONG)
                .show()
    }

}