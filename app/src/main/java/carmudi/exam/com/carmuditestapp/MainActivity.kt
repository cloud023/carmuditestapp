package carmudi.exam.com.carmuditestapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import carmudi.exam.com.carmuditestapp.presentation.car.CarProductListFragment

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //load saved fragment from instanced if any
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(R.id.frame_activity_main, CarProductListFragment(), CarProductListFragment.CAR_PRODUCT_LIST_FRAGMENT_TAG)
                    .commit()
        } else {

            supportFragmentManager.beginTransaction().replace(R.id.frame_activity_main, supportFragmentManager.findFragmentByTag(CarProductListFragment.CAR_PRODUCT_LIST_FRAGMENT_TAG) as
                    CarProductListFragment?
                    , CarProductListFragment.CAR_PRODUCT_LIST_FRAGMENT_TAG)
                    .commit()
        }


    }
}
