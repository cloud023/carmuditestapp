package carmudi.exam.com.carmuditestapp.model.car

import carmudi.exam.com.carmuditestapp.model.remote.ApiEndPoint
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Single
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit

class CarProductApi {

    companion object {

        /**
         * Change Interceptor to Facebook Stetho on which we can use the Google Chrome
         * extension to view all network related request for easy debugging
         *
         * ex. chrome://inspect
         */
        fun getOkHttpClient(): OkHttpClient {

            return OkHttpClient().newBuilder()
                    .connectTimeout(ApiEndPoint.DEFAULT_CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                    .addNetworkInterceptor(StethoInterceptor())
                    .readTimeout(ApiEndPoint.DEFAULT_READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                    .build()

        }

        /**
         * A simple rxjava + fastnetworking library request on which we can use observers to listen
         * for data requests.
         */
        fun getProductList(request: GetCarProductRequest): Single<GetCarProductResponse> {

            return Rx2AndroidNetworking
                    .get(constructGetProductListUrl(request))
                    .setOkHttpClient(getOkHttpClient())
                    .build()
                    .getObjectSingle(GetCarProductResponse::class.java)

        }

        fun constructGetProductListUrl(request: GetCarProductRequest): String {

            return ApiEndPoint.CAR_LIST + "page:${request.getPage()}/maxitems:${request.getMaxItems()}/sort:${request.getSort()}"

        }

    }


}