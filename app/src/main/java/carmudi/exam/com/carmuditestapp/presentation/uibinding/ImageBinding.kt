package carmudi.exam.com.carmuditestapp.presentation.uibinding

import com.squareup.picasso.Picasso
import android.databinding.BindingAdapter
import android.widget.ImageView
import carmudi.exam.com.carmuditestapp.R


/**
 * Binding Adapter that use by Data Binding Library
 * which utilizes Picasso library
 */
@BindingAdapter("app:imageUrl")
fun loadImage(view: ImageView, imageUrl: String) {
    Picasso.get()
            .load(imageUrl)
            .resize(120, 120)
            .centerCrop()
            .placeholder(R.drawable.baseline_directions_car_black_36)
            .into(view)
}


