package carmudi.exam.com.carmuditestapp.model.car

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.text.NumberFormat


class CarProduct {

    @SerializedName("brand")
    @ColumnInfo(name = "brand")
    private var brand = ""

    @SerializedName("original_name")
    @ColumnInfo(name = "original_name")
    private var originalName = ""

    @SerializedName("price")
    @ColumnInfo(name = "original_price")
    private var price = ""

    @Ignore
    val PHP_CURRENCY = "PHP"

    @Ignore
    val IDR_TO_PHP_RATE = 0.0038

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0

    fun getBrand(): String {
        return brand
    }

    fun getOriginalName(): String {
        return originalName
    }

    fun getOriginalPrice(): String {
        return price
    }


    fun getCarNameAndBrand(): String {
        return "$originalName - $brand"
    }

    /**
     * Create formatted PHP currency
     */
    fun getFormattedConvertedPriceToPhp(): String {
        return try {
            NumberFormat.getCurrencyInstance().format(price.toDouble() * IDR_TO_PHP_RATE)
        } catch (e: IllegalArgumentException) {
            "Price Not Available"
        }
    }

}